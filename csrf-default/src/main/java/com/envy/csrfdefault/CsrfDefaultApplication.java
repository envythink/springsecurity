package com.envy.csrfdefault;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CsrfDefaultApplication {

    public static void main(String[] args) {
        SpringApplication.run(CsrfDefaultApplication.class, args);
    }

}
