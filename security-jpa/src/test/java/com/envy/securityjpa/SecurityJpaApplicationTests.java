package com.envy.securityjpa;

import com.envy.securityjpa.entity.Role;
import com.envy.securityjpa.entity.User;
import com.envy.securityjpa.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class SecurityJpaApplicationTests {
    @Autowired
    private UserRepository userRepository;

    @Test
    void contextLoads() {
        User user1 = new User();
        user1.setUsername("envy");
        user1.setPassword("1234");
        user1.setAccountNonExpired(true);
        user1.setAccountNonLocked(true);
        user1.setCredentialsNonExpired(true);
        user1.setEnabled(true);

        List<Role> roles1 = new ArrayList<>();
        Role r1 = new Role();
        r1.setName("ROLE_admin");
        r1.setDescription("管理员");

        roles1.add(r1);
        user1.setRoles(roles1);
        userRepository.save(user1);


        User user2 = new User();
        user2.setUsername("test");
        user2.setPassword("1234");
        user2.setAccountNonExpired(true);
        user2.setAccountNonLocked(true);
        user2.setCredentialsNonExpired(true);
        user2.setEnabled(true);

        List<Role> roles2 = new ArrayList<>();
        Role r2 = new Role();
        r2.setName("ROLE_user");
        r2.setDescription("普通用户");

        roles2.add(r2);
        user2.setRoles(roles2);
        userRepository.save(user2);
    }
}
