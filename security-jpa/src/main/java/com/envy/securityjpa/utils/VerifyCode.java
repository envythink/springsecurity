package com.envy.securityjpa.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

public class VerifyCode {
    //定义生成验证码图片的宽度
    private int width = 100;
    //定义生成验证码图片的高度
    private int height = 50;
    //定义生成验证码的字体
    private String[] fontName = { "宋体", "楷体", "隶书", "微软雅黑"};
    //定义生成验证码图片的背景颜色,此处为白色
    private Color bgColor = new Color(255,255,255);
    //定义生成验证码中字符的取值范围
    private String codes = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    //记录生成验证码中的字符串
    private String text;
    //定义一个随机对象
    private Random random = new Random();
    //定义验证码中字符的个数,此处为4个
    private int charNumber = 4;
    //定义验证码中干扰线的条数，一般会比字符多一些，此处为6条
    private int lineNumber = 6;


    /**
     * 获取一个随机字符颜色
     * */
    private Color randomColor(){
        //注意颜色取值在[0,255]之间，考虑效果这里设置为[0.200]
        int red = random.nextInt(200);
        int green = random.nextInt(200);
        int blue = random.nextInt(200);
        return new Color(red,green,blue);
    }

    /**
     * 获取一个随机字符字体
     * */
    private Font randomFont(){
        //Font(name,style,size)，其中name为字体名称,style为风格，size为字号
        //随机获取前面设置的字体名称
        String name = fontName[random.nextInt(fontName.length)];
        int style = random.nextInt(4);
        int size = random.nextInt(5)+25;
        return new Font(name,style,size);
    }

    /**
     * 获取一个随机字符
     * */
    private char randomChar(){
        return codes.charAt(random.nextInt(codes.length()));
    }

    /**
     * 获取生成验证码中的字符串
     * */
    public String getText(){
        return text;
    }

    /**
     * 绘制干扰线
     * */
    private void drawLine(BufferedImage bufferedImage){
        Graphics2D g2 = (Graphics2D)bufferedImage.getGraphics();
        for(int i=0;i<lineNumber;i++){
            //两点确定一条直线，因此x1,y1(x2,y2)确定起点(终点)的横纵坐标,且取值均在验证码图片范围内
            int x1 = random.nextInt(width);
            int y1 = random.nextInt(height);
            int x2 = random.nextInt(width);
            int y2 = random.nextInt(height);
            //设置线条颜色
            g2.setColor(randomColor());
            //设置线条粗细
            g2.setStroke(new BasicStroke(1.5f));
            g2.drawLine(x1,y1,x2,y2);
        }
    }

    /**
     * 创建一个空白的BufferedImage对象
     * */
    private BufferedImage createBufferedImage(){
        BufferedImage bufferedImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = (Graphics2D)bufferedImage.getGraphics();
        //设置验证码图片的背景颜色
        g2.setColor(bgColor);
        g2.fillRect(0,0,width,height);
        return bufferedImage;
    }

    /**
     * 获取一个BufferedImage对象
     * */
    public BufferedImage getBufferedImage(){
        BufferedImage bufferedImage = createBufferedImage();
        Graphics2D g2 = (Graphics2D)bufferedImage.getGraphics();
        StringBuffer stringBuffer = new StringBuffer();
        for(int i =0;i<charNumber;i++){
            //随机获得一个字符
            String s = randomChar()+"";
            stringBuffer.append(s);
            //设置当前获得字符的颜色
            g2.setColor(randomColor());
            //设置当前获得字符的字体
            g2.setFont(randomFont());
            float x = i * width*1.0f/4;
            //依据样式绘制当前获得的字符
            g2.drawString(s,x,height - 15);
        }
        //给BufferedImage对象设置字符串
        this.text = stringBuffer.toString();
        //绘制干扰线
        drawLine(bufferedImage);
        return bufferedImage;
    }

    /**
     * 输出验证码图片
     * */
    public static void out(BufferedImage bufferedImage, OutputStream outputStream) throws IOException {
        ImageIO.write(bufferedImage,"JPEG",outputStream);
    }
}