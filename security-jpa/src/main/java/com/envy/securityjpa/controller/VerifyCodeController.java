package com.envy.securityjpa.controller;

import com.envy.securityjpa.utils.VerifyCode;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;

@RestController
public class VerifyCodeController {
    @GetMapping("/vercode")
    public void code(HttpServletRequest request, HttpServletResponse response) throws IOException {
        VerifyCode verifyCode = new VerifyCode();
        //获取验证码
        BufferedImage image = verifyCode.getBufferedImage();
        //获取验证码文字
        String text = verifyCode.getText();
        HttpSession session = request.getSession();
        session.setAttribute("verify_code",text);
        VerifyCode.out(image,response.getOutputStream());
    }
}