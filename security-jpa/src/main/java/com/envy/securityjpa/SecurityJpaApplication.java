package com.envy.securityjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@SpringBootApplication
public class SecurityJpaApplication {
    public static void main(String[] args) {
        SpringApplication.run(SecurityJpaApplication.class, args);
    }

}
