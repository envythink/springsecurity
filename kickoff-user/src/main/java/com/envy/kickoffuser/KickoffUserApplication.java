package com.envy.kickoffuser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KickoffUserApplication {
    public static void main(String[] args) {
        SpringApplication.run(KickoffUserApplication.class, args);
    }
}
