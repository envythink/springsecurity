package com.envy.csrfdanger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CsrfDangerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CsrfDangerApplication.class, args);
    }

}
