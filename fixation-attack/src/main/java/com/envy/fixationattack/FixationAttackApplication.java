package com.envy.fixationattack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.web.firewall.StrictHttpFirewall;

@SpringBootApplication
public class FixationAttackApplication {
    public static void main(String[] args) {
        SpringApplication.run(FixationAttackApplication.class, args);
    }
}
