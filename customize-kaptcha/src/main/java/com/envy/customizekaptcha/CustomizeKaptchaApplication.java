package com.envy.customizekaptcha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@SpringBootApplication
public class CustomizeKaptchaApplication {
    public static void main(String[] args) {
        SpringApplication.run(CustomizeKaptchaApplication.class, args);
    }

}
