package com.envy.customizekaptcha.config;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
public class VerifyCodeConfig {
    @Bean
    DefaultKaptcha verifyCode(){
        Properties properties = new Properties();
        //定义生成验证码图片的宽度
        properties.setProperty("kaptcha.image.width","150");
        //定义生成验证码图片的高度
        properties.setProperty("kaptcha.image.height","50");
        //定义生成验证码中字符的取值范围
        properties.setProperty("kaptcha.textproducer.char.string", "0123456789");
        //定义验证码中字符的个数,此处为4个
        properties.setProperty("kaptcha.textproducer.char.length", "4");

        Config config = new Config(properties);
        DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
        defaultKaptcha.setConfig(config);
        return defaultKaptcha;
    }
}
