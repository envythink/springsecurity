package com.envy.customizekaptcha.details;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import javax.servlet.http.HttpServletRequest;

public class MyWebAuthenticationDetails extends WebAuthenticationDetails {
    //自定义一个属性，用于判断验证码是否通过
    private boolean isPassed = false;

    //自定义请求方法属性
    private String method;

    public MyWebAuthenticationDetails(HttpServletRequest request) {
        super(request);
        //获取用户通过表单输入的验证码字符串
        String formCaptcha =request.getParameter("code");
        //获取生成的验证码字符串（从session中获取)
        String genCaptcha = (String) request.getSession().getAttribute("verify_code");
        if(formCaptcha !=null && genCaptcha !=null && formCaptcha.equals(genCaptcha)){
           isPassed =true;
        }

        //给此处的自定义请求方法属性赋值
        this.method =request.getMethod();
    }

    public boolean isPassed(){
        return this.isPassed;
    }

    public String getMethod(){
        return this.method;
    }
}
