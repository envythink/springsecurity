package com.envy.customizekaptcha.service;

import com.envy.customizekaptcha.details.MyWebAuthenticationDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Service;

@Service
public class InformationService {
    public String info(){
        Authentication authentication =SecurityContextHolder.getContext().getAuthentication();
        WebAuthenticationDetails details = (WebAuthenticationDetails)authentication.getDetails();
        String remoteAddress = details.getRemoteAddress();
        String sessionId = details.getSessionId();
        return String.format("remoteAddress is:"+remoteAddress+",sessionId is:"+sessionId);
    }

    public String customizeInfo(){
        Authentication authentication =SecurityContextHolder.getContext().getAuthentication();
        MyWebAuthenticationDetails details = (MyWebAuthenticationDetails)authentication.getDetails();
        String remoteAddress = details.getRemoteAddress();
        String sessionId = details.getSessionId();
        String method = details.getMethod();
        return String.format("remoteAddress is:"+remoteAddress+",sessionId is:"+sessionId+",method is:"+method);
    }
}
