package com.envy.customizekaptcha.controller;

import com.envy.customizekaptcha.service.InformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InformationController {
    @Autowired
    private InformationService informationService;

    @GetMapping("/info")
    public String info(){
        return informationService.info();
    }

    @GetMapping("/customizeInfo")
    public String customizeInfo(){
        return informationService.customizeInfo();
    }
}
