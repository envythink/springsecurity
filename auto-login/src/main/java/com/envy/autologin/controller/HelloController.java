package com.envy.autologin.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @GetMapping("/hello")
    public String hello(){
        return "Hello,World!";
    }

    @GetMapping("/book")
    public String book(){
        return "Hello,Book!";
    }

    @GetMapping("/movie")
    public String movie(){
        return "Hello,Movie!";
    }
}
