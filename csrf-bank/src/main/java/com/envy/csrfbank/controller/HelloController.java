package com.envy.csrfbank.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/index")
    public String index(){
        return "the index page!";
    }

    @PostMapping("/transfer")
    public String transferMoney(String username,String money){
        return String.format("username is "+username+",money is "+money);
    }
}
