package com.envy.csrfbank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CsrfBankApplication {

    public static void main(String[] args) {
        SpringApplication.run(CsrfBankApplication.class, args);
    }

}
