package com.envy.csrfend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;

@SpringBootApplication
public class CsrfEndApplication {
    public static void main(String[] args) {
        SpringApplication.run(CsrfEndApplication.class, args);
    }
}
