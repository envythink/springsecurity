package com.envy.securitysession;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.session.security.SpringSessionBackedSessionRegistry;

@SpringBootApplication
public class SecuritySessionApplication {
    public static void main(String[] args) {
        SpringApplication.run(SecuritySessionApplication.class, args);
    }

}
