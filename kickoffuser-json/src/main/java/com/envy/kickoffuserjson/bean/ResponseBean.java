package com.envy.kickoffuserjson.bean;

public class ResponseBean {
    private Integer status;
    private String message;
    private Object object;

    private ResponseBean() {
    }

    private ResponseBean(Integer status, String message, Object object) {
        this.status = status;
        this.message = message;
        this.object = object;
    }

    public static ResponseBean build(){
        return new ResponseBean();
    }

    //成功，只有信息
    public static ResponseBean ok(String message){
        return new ResponseBean(200,message,null);
    }

    //成功，有信息和数据
    public static ResponseBean ok(String message,Object object){
        return new ResponseBean(200,message,object);
    }

    //失败，只有信息
    public static ResponseBean error(String message){
        return new ResponseBean(500,message,null);
    }

    //失败，有信息和数据
    public static ResponseBean error(String message,Object object){
        return new ResponseBean(500,message,object);
    }

    public Integer getStatus() {
        return status;
    }

    public ResponseBean setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ResponseBean setMessage(String message) {
        this.message = message;
        return this;
    }

    public Object getObject() {
        return object;
    }

    public ResponseBean setObject(Object object) {
        this.object = object;
        return this;
    }
}
