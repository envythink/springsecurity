package com.envy.kickoffuserjson.repository;

import com.envy.kickoffuserjson.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
    User findUserByUsername(String username);
}
