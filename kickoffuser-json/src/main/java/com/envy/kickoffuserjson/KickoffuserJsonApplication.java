package com.envy.kickoffuserjson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KickoffuserJsonApplication {
    public static void main(String[] args) {
        SpringApplication.run(KickoffuserJsonApplication.class, args);
    }
}
