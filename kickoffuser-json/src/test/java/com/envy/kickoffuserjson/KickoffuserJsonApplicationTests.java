package com.envy.kickoffuserjson;

import com.envy.kickoffuserjson.entity.User;
import com.envy.kickoffuserjson.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class KickoffuserJsonApplicationTests {

    @Autowired
    private UserRepository userRepository;

    @Test
    void contextLoads() {
        User user = new User();
        user.setId(Long.parseLong("1"));
        user.setUsername("envy");
        user.setPassword("1234");
        userRepository.save(user);
    }

}
