package com.envy.ssone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsoneApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsoneApplication.class, args);
    }

}
