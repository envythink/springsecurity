package com.envy.securityfirewall.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@RestController
public class HelloController {
    @GetMapping("/hello")
    public String hello(){
        return "hello,world!";
    }

    @GetMapping("/world")
    public String world(@RequestParam("sex") String sex){
        return sex;
    }

    @GetMapping("/movie/{id}")
    public String movie(@PathVariable String id, @MatrixVariable String name){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        System.out.println("requestURI is:"+request.getRequestURI());
        System.out.println("contextPath is:"+request.getContextPath());
        System.out.println("servletPath is:"+request.getServletPath());
        System.out.println("pathInfo is:"+request.getPathInfo());
        return String.format("id is:" +id+", name is:"+name);
    }
}
