package com.envy.securityfirewall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.web.firewall.HttpFirewall;

import javax.servlet.http.HttpSession;

@SpringBootApplication
public class SecurityFirewallApplication {
    public static void main(String[] args) {
        SpringApplication.run(SecurityFirewallApplication.class, args);
    }

}
