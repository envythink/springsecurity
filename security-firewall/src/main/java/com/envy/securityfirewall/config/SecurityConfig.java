package com.envy.securityfirewall.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;


@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Bean
    HttpFirewall httpFirewall(){
        StrictHttpFirewall strictHttpFirewall = new StrictHttpFirewall();
//        strictHttpFirewall.setUnsafeAllowAnyHttpMethod(true);
//        strictHttpFirewall.setAllowSemicolon(true);
//        strictHttpFirewall.setAllowUrlEncodedDoubleSlash(true);
//        strictHttpFirewall.setAllowUrlEncodedPercent(true);
//        strictHttpFirewall.setAllowUrlEncodedSlash(true);
//        strictHttpFirewall.setAllowBackSlash(true);
        strictHttpFirewall.setAllowUrlEncodedPeriod(true);
        return strictHttpFirewall;
    }
}
