package com.envy.securitycaptcha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityCaptchaApplication {
    public static void main(String[] args) {
        SpringApplication.run(SecurityCaptchaApplication.class, args);
    }

}
