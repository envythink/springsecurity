package com.envy.kickoffuserjpa.repository;


import com.envy.kickoffuserjpa.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
    User findUserByUsername(String username);
}
