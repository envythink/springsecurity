package com.envy.kickoffuserjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KickoffuserJpaApplication {
    public static void main(String[] args) {
        SpringApplication.run(KickoffuserJpaApplication.class, args);
    }

}
